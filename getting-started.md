# Primeros pasos

[Home](README.md) | Primeros pasos

## Seguridad

Para autenticar una petición se utilizará **OAuth 2.0**.

Antes de poder utilizar la wherEX API, tendrá que estar registrado como comprador y solicitar su `client_id` y `client_secret`.

Cada petición deberá ir acompañada de un *token de acceso* en la cabecera. 

> Esta credencial es única y confidencial por empresa comprador.

## Obtener un token de acceso

Se debe realizar una petición `POST` enviando su `client_id`, `client_secret` e indicando que los datos proporcionados son sus credenciales mediante `grant_type=client_credentials`.

**Ejemplo**

```bash
curl -X POST https://dev-api.wherex.com/oauth/v2/token \
    -F client_id=d015cc465bdb4e51987df7fb870472d3fb9a3505 \
    -F client_secret=d015cc465bdb4e51987df7fb870472d3fb9a3505 \
    -F grant_type=client_credentials
```

**Respuesta**

```json
{
    "access_token": "0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11",
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": null
}
```

## Hacer una petición

Una vez que haya obtenido el *token de acceso*, realice una petición enviando el `access_token` en la cabecera de la aplicación como se muestra a continuación:

**Ejemplo**

```bash
curl -X GET https://dev-api.wherex.com/v1/products \
    -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

**Respuesta**

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "sku": "11001100",
            "name": "BMW 330i Sedán",
            "type": "B",
            "comment": "5 Puertas",
            "category": {
                "code": 1010,
                "name": "Vehiculos"
            },
            "files": [
                {
                    "name": "informacion-complementaria.pdf",
                    "link": "https://dev-api.wherex.com/v1/files/aW5mb3JtYWNpb24tY29tcGxlbWVudGFyaWEucGRm"
                }
            ]
        }
    ]
}
```

## A continuación...

[Paso a paso de Integración](integration.md)
