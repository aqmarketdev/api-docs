# Ofertas

[Home](../README.md) | Ofertas

## Descripción

Liste y busque ofertas enviadas por los proveedores a las solicitudes de productos.

## Métodos

- [Listar](list.md)
