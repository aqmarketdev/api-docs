# Listar Ofertas

[Home](../README.md) | [Ofertas](index.md) | Listar

## Descripción

Este método le permite listar sus solicitudes de productos y sus respectivas ofertas.

## Petición

```
curl -X GET http://dev-api.wherex.com/v1/proposals \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetros opcionales

| Parámetros          | Tipo    | Descripción                               | Valores admitidos
| ---                 | ---     | ---                                       | ---
| filter[requestId]   | string  | Filtrar por código de solicitud           |
| filter[requestAt]   | date    | Filtrar por fecha de solicitud            | ISO-8601 (yyyy-mm-dd)
| filter[bidId]       | int     | Filtrar por código de licitación          |
| filter[bidStatus]   | string  | Filtrar por estado de licitación          |
| filter[bidClosedAt] | date    | Filtrar por fecha de cierre de licitación | ISO-8601 (yyyy-mm-dd)
| order[requestId]    | string  | Ordenar por código de solicitud           | ASC/DESC
| order[requestAt]    | date    | Ordenar por fecha de solicitud            | ASC/DESC
| order[bidId]        | int     | Ordenar por código de licitación          | ASC/DESC
| order[bidStatus]    | string  | Ordenar por estado de licitación          | ASC/DESC
| order[bidClosedAt]  | date    | Ordenar por feche de cierre de licitación | ASC/DESC
| limit               | integer | Ofertas por página                        | 1 - 20
| page                | integer | Página                                    |

### Ejemplos

```
GET http://dev-api.wherex.com/v1/proposals
GET http://dev-api.wherex.com/v1/proposals?limit=10&page=1
GET http://dev-api.wherex.com/v1/proposals?limit=10&page=1&filter[requestId]=ZXCV0987
GET http://dev-api.wherex.com/v1/proposals?limit=10&page=1&order[requestAt]=ASC
```

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "request": {
                "wherexId":  
                "requestId": "",
                "productRequestId": 
                "requestAt": ""
            },
            "bid": {
                "bidId": 1,
                "status": "finished",
                "closedAt": "2019-01-25T16:05:01-03:00"
            },
            "proposalItems": [
                {
                    "proposal": {
                        "proposalId": 400200,
                        "sentAt": "2019-01-25T16:05:01-03:00",
                        "completenessDiscount": 2,
                        "deliveryEstimate": 10,
                        "deliveryRespect": true,
                        "handOutLocation": "",
                        "proposalDuration": "30",
                        "comment": "Despacho a domicilio.",
                        "files": []
                    },
                    "supplier": {
                        "rut": "11223344-3",
                        "name": "Proveedor",
                        "contact": {
                            "name": "",
                            "email": "proveedor@dominio.cl",
                            "phone": "+56212341234"
                        }
                    },
                    "currency": {
                        "code": "clp",
                        "name": "Pesos"
                    },
                    "product": {
                        "sku": "",
                        "description": "",
                        "comment": "",
                        "files": []
                    },
                    "proposalItemId": 101101,
                    "description": "",
                    "comments": "",
                    "status": "accepted",
                    "quantityOffered": 100,
                    "quantityAdjudicated": 100,
                    "unitPriceOffered": 50,
                    "unitPricePaid": 50,
                    "totalPriceOffered": 5000,
                    "totalPricePaid": 5000,
                    "unitHasEquivalence": false,
                    "unitEquivalence": "",
                    "unitQuantity": 0,
                    "unitMeasurementUnit": "",
                    "files": []
                },
                {...},
                {...}                
            ]
        },
        {...},
        {...}
    ],
    "metadata": {
            "total": 17320,
            "page": "1",
            "limit": 20
    }
}
```

## Estructura de la Respuesta

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| code                 | integer | Código HTTP
| message              | string  | Descripción del código HTTP
| data                 | array   | Arreglo de datos

### Estructura de *data*

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| request              | object  | Solicitud de producto
| bid                  | object  | Licitación
| proposalItems        | array   | Ofertas de proveedores

### Estructura de *request*

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| wherexId             | string  | Código interno de la solicitud
| requestId            | string  | Código de solicitud
| productRequestId     | int     | Número de la posición en la solicitud. 
| requestDate          | string  | Fecha de solicitud


### Estructura de *bid*

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| bidId                | integer | Correlativo de licitación
| status               | string  | Estado de licitación

### Estructura de *proposalItems*

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| proposal             | object  | Oferta de proveedor
| supplier             | object  | Proveedor
| currency             | object  | Moneda
| product              | object  | Producto de proveedor
| proposalItemId       | integer | Código de oferta de proveedor
| description          | string  | Descripción de oferta
| comments             | string  | Comentarios de oferta
| status               | string  | Estado de oferta
| quantityOffered      | float   | Cantidad ofertada
| quantityAdjudicated  | float   | Cantidad adjudicada
| unitPriceOffered     | float   | Precio unitario ofertado
| unitPricePaid        | float   | Precio unitario pagado
| totalPriceOffered    | float   | Precio total ofertado
| totalPricePaid       | float   | Precio total pagado
| unitHasEquivalence   | boolean | Medida equivalente de oferta
| unitEquivalence      | string  | Factor equivalente de ofertada
| unitQuantity         | float   | Cantidad equivalente de ofertada
| unitMeasurementUnit  | string  | Unidad de medida equivalente de oferta
| files                | array   | Archivos de oferta

### Estructura de *proposal*

| Propiedad            | Tipo    | Descripción
| ---                  | ---     | ---
| sentAt               | date    | Fecha de envío de propuesta
| completenessDiscount | string  | Porcentaje de descuento aplicado a la adjudicación de toda la oferta
| deliveryEstimate     | integer | Cantidad de días hábiles para la entrega del producto
| deliveryRespect      | boolean | Entrega en lugar establecido
| handOutLocation      | string  | Lugar de despacho ofertado
| proposalDuration     | string  | Plazo de validez de oferta
| comment              | string  | Comentario general de oferta
| files                | array   | Archivos generales de oferta

### Estructura de *supplier*

| Propiedad            | Tipo    | Descripción
| ---                  |  ---    | ---
| rut                  | string  | Rut de proveedor
| name                 | string  | Nombre de proveedor
| contact              | object  | Contacto de proveedor

### Estructura de *contact*

| Propiedad            | Tipo    | Descripción
| ---                  |  ---    | ---
| name                 | string  | Nombre de contacto
| email                | string  | Correo de contacto
| phone                | string  | Teléfono de contacto

### Estructura de *currency*

| Propiedad            | Tipo    | Descripción
| ---                  |  ---    | ---
| code                 | string  | Código de moneda
| name                 | string  | Nombre de moneda

### Estructura de *product*

| Propiedad            | Tipo    | Descripción
| ---                  |  ---    | ---
| sku                  | string  | Código de producto de proveedor
| description          | string  | Descripción de producto
| comment              | string  | Comentario de producto
| files                | string  | Archivos de producto

### Estructura de *files*

| Propiedad            | Tipo    | Descripción
| ---                  |  ---    | ---
| name                 | string  | Nombre de archivo
| link                 | string  | Link de descarga
