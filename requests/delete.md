# Eliminar Solicitudes de productos

[Home](../README.md) | [Solicitudes de productos](index.md) | Eliminar

## Descripción

Este método permite Eliminar una solicitud de productos de un comprador.

> Podrá eliminar la solicitud si no ha sido utilizado en una licitación.
 
## Petición

```
curl -X DELETE http://dev-api.wherex.com/v1/products/{requestId} \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetro de la petición

| Parámetro | Tipo   | Descripción
| ---       | ---    | ---
| requestId | string | Id de la solicitud en la ERP

### Ejemplos

```
DELETE http://dev-api.wherex.com/v1/requests/3050
DELETE http://dev-api.wherex.com/v1/requests/Solicitud-1000
DELETE http://dev-api.wherex.com/v1/requests/2019-01-10-3500
```

## Respuesta

```json
{
    "code": 204,
    "message": "OK"
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
