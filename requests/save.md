# Guardar Solicitudes de pedido

[Home](../README.md) | [Solicitudes de pedido](index.md) | Guardar / Enviar

## Descripción

Este es el primer paso de la integración, con el cual se logrará enviar al portal wherEX las solicitudes de pedido. Puede incorporar tantas solicitudes de pedido como quiera por llamada

## Petición

```
curl -X POST http://dev-api.wherex.com/v1/requests \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11' \
     -H 'Content-Type: application/json' \
     -d '[
            {
                "requestId": "120091",
                "productRequestId": "120091",
                "requestDate": "2018-12-29 00:00:00",
                "productSku": "ZXCV0987",
                "productName": "Pila AAA",
                "quantity": 10,
                "measurementUnit": "Unidades",
                "unit": "Compras",
                "user": "Juan Perez",
                "costCenter": "",
                "urgencyDate": "2019-01-30 00:00:00"
                "requestComments": "Batería Alcalina de larga duración.",
                "bidComments": "Batería Alcalina de larga duración.",
                "requestedBy": "Juan Perez"
            }
        ]'
```

### Parámetros de la petición

| Propiedad        | Tipo   | Null  |Descripción
| ---              | ---    |       |---
| requestId        | string | false |Id de la solicitud en el ERP. Si utiliza SAP, este correspondería al número de solped
| productRequestId | string | false |Id del producto la solicitud en el ERP. Si utiliza SAP, este correspondería al la posición del producto en la solped
| requestDate      | date   | false |Fecha original de la solicitud. Formato ISO-8601 (yyyy-mm-dd hh:ii:ss p)
| productSku       | string | false |Código del producto
| productName      | string | false |Nombre del producto
| quantity         | float  | false |Cantidad de productos solicitados
| measurementUnit  | string | false |Unidad de la cantidad de productos solicitados
| unit             | string | false |Unidad solicitante
| user             | string | false |Nombre del usuario importador, debe ser identico al valor asignado en wherex 
| requestedBy      | string | false |Nombre del usuario que realiza la solicitud, puede contener valores no registrados en wherex
| costCenter       | string | true  |Centro de Costo (Opcional)
| urgencyDate      | date   | false |Fecha límite de la solicitud. Formato ISO-8601 (yyyy-mm-dd hh:ii:ss p)
| requestComments  | string | true  |Comentario del producto
| bidComments      | string | true  |Comentario del producto para ser publicado en la licitación

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "errors": [
            {
                "requestId": "120091",
                "error": [
                    {
                        "requestId": [
                            "Este dato es requerido."
                        ],
                        "productCode": [
                            "Este dato es requerido."
                        ]
                    }
                ]
            }
        ]
    },
    "meta": {
        "received": 3,
        "created": 1,
        "updated": 1,
        "error": 1
    }
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
| data      | array   | Arreglo de datos
| meta      | object  | Metadatos asociados a la respuesta

### Estructura de *data*

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| received  | array   | Lista de solicitudes de producto recibidos
| created   | array   | Lista de solicitudes de producto creados
| updated   | array   | Lista de solicitudes de producto actualizados
| errors    | array   | Lista de solicitudes de producto con error

### Estructura de *meta*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| received  | int     | Cantidad de solicitudes de producto recibidos en la petición
| created   | int     | Cantidad de solicitudes de producto creados con éxito
| updated   | int     | Cantidad de solicitudes de producto actualizados con éxito
| errors    | int     | Cantidad de solicitudes de producto con error

## Para el caso de que los datos de entrada sean distintos a los definidos
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "received": [
            {
                "index": 0,
                "requestId": "120095"
            }
        ],
        "errors": [
            {
                "index": 0,
                "requestId": "120095",
                "errors": {
                    "msg": [
                        "The option \"campo\" does not exist. Defined options are: \"bidComments\", \"costCenter\", \"measurementUnit\", \"productName\", \"productRequestId\", \"productSku\", \"quantity\", \"requestComments\", \"requestDate\", \"requestId\", \"unit\", \"urgencyDate\", \"user\"."
                    ]
                }
            }
        ]
    },
    "meta": {
        "received": 1,
        "errors": 1
    }
}
```

