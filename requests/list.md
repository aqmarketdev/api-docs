# Listar Solicitudes de productos

[Home](../README.md) | [Solicitudes de productos](index.md) | Listar

## Descripción

Este método le permite listar sus solicitudes de productos. 

## Petición

```
curl -X GET http://dev-api.wherex.com/v1/requests \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetros opcionales

| Parámetros          | Tipo    | Descripción                     | Valores admitidos
| ---                 | ---     | ---                             | ---
| filter[productSku]  | string  | Filtrar por código de producto  |
| filter[productName] | string  | Filtrar por nombre de producto  |
| filter[wherexId]  | string  | Filtrar por código wherex     |
| filter[requestId]   | string  | Filtrar por código de solicitud |
| filter[dateFrom]    | string  | Filtrar por fecha desde         | ISO-8601 (yyyy-mm-dd)
| filter[dateTo]      | string  | Filtrar por fecha hasta         | ISO-8601 (yyyy-mm-dd)
| order[correlative]  | string  | Ordenar por correlativo         | ASC/DESC
| order[product]      | string  | Ordenar por producto            | ASC/DESC
| order[date]         | string  | Ordenar por fecha               | ASC/DESC
| order[type]         | string  | Ordenar por tipo de producto    | ASC/DESC
| limit               | integer | Solicitudes por página          | 1 - 20
| page                | integer | Página                          |

### Ejemplos

```
GET http://dev-api.wherex.com/v1/requests
GET http://dev-api.wherex.com/v1/requests?limit=10&page=1
GET http://dev-api.wherex.com/v1/requests?limit=10&page=1&filter[productCode]=ZXCV0987
GET http://dev-api.wherex.com/v1/requests?limit=10&page=1&order[date]=DESC
```

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "wherexId": "1235",
            "requestId": "120091",
            "requestDate": "2018-12-29",
            "creationDate": "2019-10-17",
            "product": {
                "sku": "ZXCV0987",
                "name": "Pila AAA",
                "comment": "Batería Alcalina de larga duración.",
                "type": "B",
                "category": {
                    "code": 275,
                    "name": "Baterias"
                },
                "files": [
                    {
                        "name": "pila-aaa.pdf",
                        "link": "http://dev-api.wherex.com/files/971c419dd609331343dee105fffd0f4608dc0bf2"
                    }
                ]
            },
            "quantity": 10,
            "measurementUnit": "Unidades",
            "unit": "Compras",
            "user": "Juan Perez",
            "costCenter": "",
            "requestComments": "Batería Alcalina de larga duración.",
            "bidComments": "Batería Alcalina de larga duración.",
            "urgencyDate": "2019-01-30",
            "status": "Pendiente",
            "bid": {
                "correlative": 1202,
                "status": "Publicada"
            }
        },
        {...},
        {...}
    ],
    "meta": {
        "total": 35000,
        "page": 1,
        "limit": 50
    }
}
```

## Estructura de la Respuesta

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| code            | integer | Código HTTP
| message         | string  | Descripción del código HTTP
| data            | array   | Arreglo de datos
| meta            | object  | Metadatos asociados a la respuesta

### Estructura de *data*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| wherexId      | int     | Id de la solicitud wherex
| requestId       | string  | Id de la solicitud en el ERP
| requestDate     | date    | Fecha original de la solicitud. Formato ISO-8601 (yyyy-mm-dd)
| creationDate    | date    | Fecha de creación de la solicitud. Formato ISO-8601 (yyyy-mm-dd)
| product         | object  | Codigo del producto
| quantity        | float   | Cantidad de productos solicitados
| measurementUnit | float   | Unidad de la cantidad de productos solicitados
| unit            | string  | Unidad solicitante
| user            | string  | Usuario solicitante
| costCenter      | string  | Centro de Costo
| urgencyDate     | date    | Fecha límite de la solicitud. Formato ISO-8601 (yyyy-mm-dd)
| requestComments | string  | Comentario de la solicitud de producto
| bidComments     | string  | Comentario del producto para ser publicado en la licitación
| status          | string  | Estado de la solicitud.
| bid             | object  | Información de licitación. Vacío si status es "Pendiente"

### Estructura de *product*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| sku             | string  | Código de producto
| name            | string  | Nombre del producto
| comment         | string  | Comentario del producto
| type            | string  | Tipo de producto (B/S). Bien o servicio
| category        | object  | Categoría donde pertenece el producto
| files           | array   | Arreglo de archivos asociados al producto

### Estructura de *category*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| code            | string  | Código de categoría
| name            | string  | Nombre de la categoría

### Estructura de *files*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| name            | string  | Nombre de archivo
| link            | string  | Link de descarga del archivo

### Estructura de *bid*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| correlative     | integer | Correlativo de la licitación
| status          | string  | Estado de la licitación
