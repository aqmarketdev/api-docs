# Paso a paso  de Integración

[Home](README.md) | Paso a paso  de Integración

## Puntos de integración

La API cuenta con varios endpoints, pero los más importantes para lograr una integración exitosa son 3:

1. Envío de Solicitudes de pedidos desde el ERP al portal
   Para integrar este punto referirse a la sección [Solicitudes de pedido > Guardar / Enviar](requests/save.md)
2. Consulta de Ofertas para recibilas en el ERP y generar OCs
   Para integrar este punto referirse a la sección [Ofertas > Listar](proposals/list.md)
3. Carga de órdenes de compra en wherEX para que queden disponibles para proveedores
   Para integrar este punto referirse a la sección [Órdenes de compra > Guardar / Enviar](orders/save.md)

## Consideraciones

- La API permite modificar y eliminar información dependiendo si ésta aun no haya sido utilizada o sea parte de un proceso de licitación.
- La API le permite integrar su sistema y facilitar algunos procesos, no reemplazar por completo la plataforma de licitación.

## Gráfico de Flujo de Información

A continuación graficamos el Flujo de Información de un proceso de licitación.
 
Se observan los puntos de integración y los procesos que debe realizar a través del Sistema wherEX.

![Flujo de Datos](./images/resources.png)
