# Eliminar Órdenes de compra

[Home](../README.md) | [Órdenes de compra](index.md) | Eliminar

## Descripción

Este método permite eliminar una orden de compra.

> Podrá eliminar órdenes de compra que aún no hayan sido facturadas.
 
## Petición

```
curl -X DELETE http://dev-api.wherex.com/v1/products/{correlative} \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetro de la petición

| Parámetro   | Tipo   | Descripción
| ---         | ---    | ---
| correlative | string | Código o correlativo de la órden de compra

### Ejemplos

```
DELETE http://dev-api.wherex.com/v1/orders/100
DELETE http://dev-api.wherex.com/v1/orders/Lic1000
DELETE http://dev-api.wherex.com/v1/orders/2019-10-01-10500
```

## Respuesta

```json
{
    "code": 204,
    "message": "OK"
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
