# Guardar Órdenes de compra

[Home](../README.md) | [Órdenes de compra](index.md) | Guardar

## Descripción

Este método permite cargar Ordenes de compra.

> Para agregar una nueva órden de compra no deberá tener ningún error, no obstante, la actualización de items se hará, item por item.

> Se podrá dar el caso que en una misma órden de compra se hayan actualizado varios de sus items, y se hayan descartados aquellas que tengan errores de validación.

> Si una órden de compra aparece como actualiza y no cuenta con errores puede dar por hecho que todas las actualizaciones fueraon aceptadas satisfacotiamente.

## Petición

```
curl -X POST http://dev-api.wherex.com/v1/orders \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11' \
     -H 'Content-Type: application/json' \
     -d '[
            {
                "correlative": "lic-1756",
                "emittedAt": "2019-06-25 16:30:45",
                "billing": "77665544-1",
                "file": {
                    "name": "orden-lic-1756",
                    "data": "data:text/plain;base64,QVFNMDAxLUZJTEUtMDAxLnR4dAo="
                },
                "vat": 1,
                "supplier": "11223344-9",
                "items": [
                    {
                        "quantity": 1,
                        "unitPrice": 1000,
                        "proposalItemId": 123123,
                        "currency": "clp",
                        "measurementUnit": "Litros"
                    }
                ]
            },
            {
                "correlative": "lic-1750",
                "emittedAt": "2019-06-15 10:30:45",
                "billing": "77665544-1",
                "file": {
                    "name": "orden-lic-1756",
                    "data": "data:text/plain;base64,QVFNMDAxLUZJTEUtMDAxLnR4dAo="
                },
                "vat": 1,
                "supplier": "11223344-9",
                "items": [
                    {
                        "quantity": 200,
                        "unitPrice": 2000,
                        "proposalItemId": 321321,
                        "currency": "usd",
                        "measurementUnit": "Metros"
                    },
                    {
                        "quantity": 0,
                        "unitPrice": 0,
                        "proposalItemId": "454545",
                        "currency": "yen",
                        "measurementUnit": "Unidades"
                    }
                ]
            }
        ]'
```

### Parámetros de la petición

| Propiedad       | Tipo    | Requerido | Descripción
| ---             | ---     | ---       | ---
| correlative     | string  | Si        | Número o código de órden de compra
| emittedAt       | string  | No        | Fecha de emisión
| billing         | string  | No        | Rut de Unidad de Facturación
| file            | object  | Si        | Archivo de órden de compra
| vat             | boolean | Si        | Documento afecto de IVA
| supplier        | string  | Si        | Rut de proveedor
| items           | array   | Si        | Items de órden de compra
                  
### File          
                  
| Propiedad       | Tipo    | Requerido | Descripción
| ---             | ---     | ---       | ---
| name            | string  | Si        | Nombre de archivo
| data            | string  | Si        | Contenido de archivo

### Items

| Propiedad       | Tipo    | Requerido | Descripción
| ---             | ---     | ---       | ---
| quantity        | integer | float     | Cantidad comprada
| unitPrice       | string  | float     | Precio unitario
| proposalItemId  | string  | integer   | Código de Propuesta de Proveedor
| currency        | string  | string    | Moneda
| measurementUnit | float   | string    | Unidad de medida

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "received": [
            {
                "index": 0,
                "correlative": "lic-1756"
            },
            {
                "index": 1,
                "correlative": "lic-1750"
            }
        ],
        "created": [
            {
                "index": 0,
                "correlative": "lic-1756"
            }
        ],
        "updated": [
            {
                "index": 1,
                "correlative": "lic-1750"
            }
        ],
        "errors": [
            {
                "index": 1,
                "correlative": "lic-1750",
                "error": [
                    {
                        "items[1].quantity": [
                            "Este dato es requerido."
                        ],
                        "items[1].unitPrice": [
                            "Este dato es requerido."
                        ]
                    }
                ]
            }
        ]
    },
    "meta": {
        "received": 1,
        "created": 1,
        "updated": 1,
        "errors": 1
    }
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
| data      | array   | Arreglo de datos
| meta      | object  | Metadatos asociados a la respuesta

### Estructura de *data*

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| received  | array   | Lista de órdenes de compra recibidos
| created   | array   | Lista de órdenes de compra creados
| updated   | array   | Lista de órdenes de compra actualizados
| errors    | array   | Lista de órdenes de compra con error

### Estructura de *meta*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| received  | int     | Cantidad de órdenes de compra recibidos en la petición
| created   | int     | Cantidad de órdenes de compra creados con éxito
| updated   | int     | Cantidad de órdenes de compra actualizados con éxito
| errors    | int     | Cantidad de órdenes de compra con error
