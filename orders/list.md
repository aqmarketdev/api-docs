# Listar Órdenes de compra

[Home](../README.md) | [Órdenes de compra](index.md) | Listar

## Descripción

Este método le permite listar sus órdenes de compra.

## Petición

```
curl -X GET http://dev-api.wherex.com/v1/orders \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetros opcionales

| Parámetros          | Tipo    | Descripción                  | Valores admitidos
| ---                 | ---     | ---                          | ---
| filter[correlative] | string  | Filtrar por correlativo      |
| filter[sku]         | string  | Filtrar por sku de producto  |
| filter[dateFrom]    | date    | Filtrar por fecha desde      | ISO-8601 (yyyy-mm-dd)
| filter[dateTo]      | date    | Filtrar por fecha hasta      | ISO-8601 (yyyy-mm-dd)
| order[correlative]  | string  | Ordenar por correlativo      | ASC/DESC
| order[date]         | string  | Ordenar por fecha            | ASC/DESC
| limit               | integer | Órdenes de compra por página | 1 - 20
| page                | integer | Página                       |

### Ejemplos

```
GET http://dev-api.wherex.com/v1/orders
GET http://dev-api.wherex.com/v1/orders?limit=10&page=1
GET http://dev-api.wherex.com/v1/orders?limit=10&page=1&filter[correlative]=1000
GET http://dev-api.wherex.com/v1/orders?limit=10&page=1&order[date]=DESC
```

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "correlative": "49002",
            "status": "final",
            "comments": "",
            "conditions": "",
            "vat": true,
            "emittedAt": "2017-10-05T12:00:00-03:00",
            "emittedBy": "1 wherex Fiordo Austral",
            "approvedAt": "2017-12-14T18:35:23-03:00",
            "approvedBy": "Helen  Hernandez",
            "file": {
                "name": "pedido_compra_n49002_apro_ltda.pdf",
                "link": "http://dev-api.wherex.com/v1/files/am92R2d5TXdhUGFoT2g1aHhsZE5GODZYNkVuT0tTcHRZSlZrOFVEOGVROD0="
            },
            "supplier": {
                "rut": "86887200-4",
                "name": "APRO LTDA",
                "contact": {
                    "name": "Pablo Chamy Elizalde",
                    "email": "pablo.chamy@apro.cl",
                    "phone": "65 2 347164"
                }
            },
            "orderItems": [
                {
                    "product": {
                        "sku": "11120137",
                        "name": "Casco azul"
                    },
                    "measurementUnit": "Unidades",
                    "quantity": 5,
                    "unitPrice": 3800,
                    "currency": {
                        "code": "clp",
                        "name": "Pesos"
                    }
                },
                {...}
            ]
        },
        {...},
        {...}
    ]
}
```

## Estructura de la Respuesta

| Propiedad       | Tipo    | Descripción
| ---             | ---     | --- 
| code            | integer | Código HTTP
| message         | string  | Descripción del código HTTP
| data            | array   | Arreglo de datos

### Estructura de *data*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| correlative     | string  | Código de órden de compra
| status          | string  | Estado de órden de compra
| comments        | string  | Comentarios de órden de compra
| conditions      | string  | Condiciones de órden de compra
| vat             | boolean | Afecto a IVA
| emittedAt       | string  | Fecha de emisión
| emittedBy       | string  | Emitido por
| approvedAt      | string  | Fecha de aprobación
| approvedBy      | string  | Aprobado por
| file            | object  | Archivo de órden de compra
| supplier        | object  | Proveedor
| orderItems      | array   | Items de órden de compra

### Estructura de *file*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| name            | string  | Nombre de archivo
| link            | string  | Link de descarga

### Estructura de *supplier*

| Propiedad       | Tipo    | Descripción
| ---             |  ---    | ---
| rut             | string  | Rut de proveedor
| name            | string  | Nombre de proveedor
| contact         | object  | Contacto de proveedor

### Estructura de *contact*

| Propiedad       | Tipo    | Descripción
| ---             |  ---    | ---
| name            | string  | Nombre de contacto
| email           | string  | Correo de contacto
| phone           | string  | Teléfono de contacto

### Estructura de *orderItems*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| product         | object  | Producto
| measurementUnit | string  | Unidad de medida
| quantity        | float   | Cantidad 
| unitPrice       | float   | Precio unitario
| currency        | string  | Moneda

### Estructura de *product*

| Propiedad       | Tipo    | Descripción
| ---             | ---     | ---
| sku             | string  | Código de producto
| name            | string  | Nombre de producto

### Estructura de *currency*

| Propiedad        | Tipo    | Descripción
| ---              |  ---    | ---
| code             | string  | Código de moneda
| name             | string  | Nombre de moneda
