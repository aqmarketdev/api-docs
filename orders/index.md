# Órdenes de compra

[Home](../README.md) | Órdenes de compra

## Descripción

Liste, busque, guarde y elimine órdenes de compra.

## Métodos

- [Listar](list.md)
- [Guardar](save.md)
- [Eliminar](delete.md)
