![wherex](./images/logo.png)

# Home

Te damos la bienvenida a la documentación de la **wherEX API**.

Esta API te permitirá integrar tu sistema empresarial con nuestra plataforma de una manera sencilla y programática utilizando las solicitudes HTTP convencionales. 

Los recursos y métodos son muy intuitivos y potentes, lo que te permitirá realizar llamadas fácilmente para recuperar información o ejecutar acciones.

A continuación te presentamos el contenido de la documentación.

## Introducción

- [Primeros pasos](getting-started.md)
- [Integración](integration.md)
- [PostmanCollection](API_2.0_dev-api.wherex.com.postman_collection.json)

## Peticiones

- [Productos](products/index.md)

    - [Listar](products/list.md)
    - [Guardar / Enviar](products/save.md)
    - [Eliminar](products/delete.md)
    - [Guardar archivos](products/files/save.md)
    - [Eliminar archivos](products/files/delete.md)

- [Solicitudes de pedido](requests/index.md)

    - [Listar](requests/list.md)
    - [Guardar / Enviar](requests/save.md)
    - [Eliminar](requests/delete.md)

- [Ofertas](proposals/index.md)

    - [Listar](proposals/list.md)

- [Órdenes de compra](orders/index.md)

    - [Listar](orders/list.md)
    - [Guardar / Enviar](orders/save.md)
    - [Eliminar](orders/delete.md)
