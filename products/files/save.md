# Guardar archivos de Productos

[Home](../../README.md) | [Productos](../index.md) | Guardar archivos

## Descripción

Este método permite guardar y actualizar hasta 10 archivos de productos con un máximo total de 25MB.

> Puede reemplazar un archivo envíando el nuevo contenido manteniendo el nombre del archivo que desea reemplazar.

## Petición

```
curl -X POST http://dev-api.wherex.com/v1/products/{sku}/files \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11' \
     -H 'Content-Type: application/json' \
     -d '[
             {
                 "name": "manual.pdf",
                 "data": "data:text/plain;base64,QVFNMDAxLUZJTEUtMDAxLnR4dAo="
             },
             {
                 "name": "especificaciones.doc",
                 "data": "data:text/plain;base64,QVFNMDAxLUZJTEUtMDAyLnR4dAo="
             },
             {
                 "name": "normas-de-seguridad.pdf"
             }
        ]'
```

### Parámetros de la petición

| Propiedad | Tipo   | Requerido | Descripción
| ---       | ---    | ---       | ---
| sku       | string | Si        | Código de product
| name      | string | Si        | Nombre del archivo
| data      | string | Si        | Archivo en base64

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "received": [
            {
                "index": 0,
                "name": "manual.pdf"
            },
            {
                "index": 1,
                "name": "especificaciones.doc"
            },
            {
                "index": 2,
                "name": "normas-de-seguridad.pdf"
            }
        ],
        "created": [
            {
                "index": 0,
                "name": "manual.pdf"
            }
        ],
        "updated": [
            {
                "index": 1,
                "name": "especificaciones.doc"
            }
        ],
        "errors": [
            {
                "index": 2,
                "name": "normas-de-seguridad.pdf",
                "error": [
                    {
                        "data": [
                            "Este dato es requerido."
                        ]
                    }
                ]
            }
        ]
    },
    "meta": {
        "received": 3,
        "created": 1,
        "updated": 1,
        "errors": 1
    }
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
| data      | array   | Arreglo de datos
| meta      | object  | Metadatos asociados a la respuesta

### Estructura de *data*

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| received  | array   | Lista de archivos de producto recibidos
| created   | array   | Lista de archivos de producto creados
| updated   | array   | Lista de archivos de producto actualizados
| errors    | array   | Lista de archivos de producto con error

### Estructura de *meta*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| received  | int     | Cantidad de archivos de producto recibidos en la petición
| created   | int     | Cantidad de archivos de producto creados con éxito
| updated   | int     | Cantidad de archivos de producto actualizados con éxito
| errors    | int     | Cantidad de archivos de producto con error

### Código de error
 3000 => 'Error al tratar de guardar el archivo.',
 3001 => 'El nombre del archivo no es válido.',
 3002 => 'No se puede hallar el archivo con el nombre indicado.',
 3003 => 'Error al tratar de eliminar el archivo',
 3004 => 'Archivo vacío',