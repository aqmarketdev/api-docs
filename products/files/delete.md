# Eliminar archivos de Productos

[Home](../../README.md) | [Productos](../index.md) | Eliminar archivos

## Descripción

Este método permite eliminar archivos asociados a un producto.
 
## Petición

```
curl -X DELETE http://dev-api.wherex.com/v1/products/{sku}/files/{name} \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetros de la petición

| Parámetros | Tipo   | Descripción
| ---        | ---    | ---
| sku        | string | Código de producto
| name       | string | Nombre de archivo

### Ejemplos

```
DELETE http://dev-api.wherex.com/v1/products/1/files/manual.pdf
DELETE http://dev-api.wherex.com/v1/products/AXD405/files/normas-de-seguridad.pdf
DELETE http://dev-api.wherex.com/v1/products/rfcj45/files/especificaciones.doc
```

## Respuesta

```json
{
    "code": 204,
    "message": "OK"
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
