# Productos

[Home](../README.md) | Productos

## Descripción

Liste, busque, guarde y elimine productos y archivos asociados a sus productos.

## Métodos

- [Listar](list.md)
- [Guardar](save.md)
- [Eliminar](delete.md)
- [Guardar archivos](files/save.md)
- [Eliminar archivos](files/delete.md)
