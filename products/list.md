# Listar Productos

[Home](../README.md) | [Productos](index.md) | Listar

## Descripción

Este método le permite listar sus productos.

## Petición

```
curl -X GET http://dev-api.wherex.com/v1/products \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetros opcionales

| Parámetros   | Tipo    | Descripción                    | Valores admitidos
| ---          | ---     | ---                            | ---
| filter[sku]  | string  | Filtrar por sku de producto    |
| filter[name] | string  | Filtrar por nombre de producto |
| filter[type] | string  | Filtrar por tipo de producto   | B/S
| order[sku]   | string  | Ordenar por sku de producto    | ASC/DESC
| order[name]  | string  | Ordenar por nombre de producto | ASC/DESC
| order[type]  | string  | Ordenar por tipo de producto   | ASC/DESC
| limit        | integer | Productos por página           | 1 - 20
| page         | integer | Página                         |

### Ejemplos

```
GET http://dev-api.wherex.com/v1/products
GET http://dev-api.wherex.com/v1/products?limit=10&page=1
GET http://dev-api.wherex.com/v1/products?limit=10&page=1&filter[sku]=ZXCV0987
GET http://dev-api.wherex.com/v1/products?limit=10&page=1&order[name]=DESC
```

## Respuesta

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "sku": "ZXCV0987",
            "name": "Pila AAA",
            "comment": "Batería Alcalina de larga duración.",
            "type": "B",
            "category": {
                "code": 275,
                "name": "Baterias"
            },
            "files": [
                {
                    "name": "pila-aaa.pdf",
                    "link": "http://dev-api.wherex.com/files/971c419dd609331343dee105fffd0f4608dc0bf2"
                }
            ]
        },
        {...},
        {...}
    ],
     "meta": {
        "total": 100,
        "page": 1,
        "limit": 20
    }
}
```
## Respuesta de exito para el caso que no se encuentren registros según los filtros ingresados

```json
    {
    "code": 200,
    "message": "OK",
    "data": [],
    "meta": {
        "total": 0,
        "page": 1,
        "limit": 20
    }
```
## Respuesta de error para los casos de ingreso de  opciones distintas a : limit  o page

```json
{
    "code": 400,
    "message": "Bad Request",
    "errors": [
        "The option \"limit\" with value \"a\" is expected to be of type \"numeric\", but is of type \"string\"."
    ]
}
```
## Respuesta de error para los casos de ingreso de  opciones tengan valores incorrectos
```json
{
    "code": 400,
    "message": "Bad Request",
    "errors": [
        "The option \"page\" with value \"-1\" is invalid."
    ]
}
```
## Respuesta de error para los casos de ingreso de filtros distintos a los especificados
```json
{
    "code": 400,
    "message": "Bad Request",
    "errors": [
        "The option \"otro\" does not exist. Defined options are: \"name\", \"sku\", \"type\"."
    ]
}
```


## Estructura de la Respuesta exitosa

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
| data      | array   | Arreglo de datos

### Estructura de *data*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| sku       | string  | Código de producto
| name      | string  | Nombre de producto
| comment   | string  | Comentario de producto
| type      | string  | Tipo de producto. Bien o servicio (B/S) 
| category  | object  | Categoría de producto
| files     | array   | Archivos de producto

### Estructura de *category*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | string  | Código de categoría
| name      | string  | Nombre de categoría

### Estructura de *files*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| name      | string  | Nombre de archivo
| link      | string  | Link de descarga

### Estructura de *meta*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| total     | string  | total de registros encontrados según los parametros indicados
| page      | int  | La página visualizada
| limit     | int  | Limite de registros a retornar en la petición