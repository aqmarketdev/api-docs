# Guardar Productos

[Home](../README.md) | [Productos](index.md) | Guardar

## Descripción

Este método permite guardar y actualizar hasta 100 productos.

> Todos los productos agregados deben pasar por una aprobación por parte de wherex, para asignarle la categoría correspondiente.

> Si el producto ya existe podrá actualizar el comentario.

## Petición

```
curl -X POST http://dev-api.wherex.com/v1/products \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11' \
     -H 'Content-Type: application/json' \
     -d '[
            {
                "sku": 100,
                "name": "Soda Caustica",
                "comment": "100%",
                "type":"B"
            },
            {
                "sku": 200,
                "name": "Soda Caustica",
                "comment": "25%"
                "type":"B"
            },
            {
                "sku": 300
            }
        ]'
```

### Parámetros de la petición

| Propiedad | Tipo   | Requerido | Descripción
| ---       | ---    | ---       | ---
| sku       | string | Si        | Código de producto
| name      | string | Si        | Nombre de producto
| type      | string | Si        | Tipo de producto  "B" (Bien)  o "S" (Servicio) 
| comment   | string | No        | Comentario de producto



## Respuesta(

```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "received": [
            {
                "index": 0,
                "sku": 100
            },
            {
                "index": 1,
                "sku": 200
            },
            {
                "index": 2,
                "sku": 300
            }
        ],
        "created": [
            {
                "index": 0,
                "sku": 100
            }
        ],
        "updated": [
            {
                "index": 1,
                "sku": 200
            }
        ],
        "errors": [
            {
                "index": 2,
                "sku": 300,
                "error": [
                    {
                        "name": [
                            "Este dato es requerido."
                        ]
                    }
                ]
            }
        ]
    },
    "meta": {
        "received": 3,
        "created": 1,
        "updated": 1,
        "errors": 1
    }
}
```

## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
| data      | array   | Arreglo de datos
| meta      | object  | Metadatos asociados a la respuesta

### Estructura de *data*

| Propiedad | Tipo    | Descripción
| ----------| --------| ---
| received  | array   | Lista de productos recibidos
| created   | array   | Lista de productos creados
| updated   | array   | Lista de productos actualizados
| errors    | array   | Lista de productos con error

### Estructura de *meta*

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| received  | int     | Cantidad de productos recibidos en la petición
| created   | int     | Cantidad de productos creados con éxito
| updated   | int     | Cantidad de productos actualizados con éxito
| errors    | int     | Cantidad de productos con error

## Respuesta en caso de error en el formato de entrada
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "errors": [
            {
                "index": 0,
                "sku": null,
                "errors": {
                    "1001": "El formato de la petición no es correcto."
                }
            }
        ]
    },
    "meta": {
        "errors": 1
    }
}
```
## Respuesta en caso de datos de entrada obligatorios vacíos. 
```json
{
    "code": 400,
    "message": "Parámetros obligatorios vacíos. ",
    "errors": []
}
```


