# Eliminar Productos

[Home](../README.md) | [Productos](index.md) | Eliminar

## Descripción

Este método permite eliminar un producto.

> Podrá eliminar productos que no hayan participado en una licitación.
 
## Petición

```
curl -X DELETE http://dev-api.wherex.com/v1/products/{sku} \
     -H 'Authorization: Bearer 0c3d46946bbf99c8213dd7f6c640ed6433bdc056a5b68e7e80f5525311b0ca11'
```

### Parámetro de la petición

| Parámetro | Tipo   | Descripción
| ----      | ---    | ---
| sku       | string | Código de producto

### Ejemplos

```
DELETE http://dev-api.wherex.com/v1/products/1
DELETE http://dev-api.wherex.com/v1/products/AXD405
DELETE http://dev-api.wherex.com/v1/products/rfcj45
```

## Respuesta

```json
{
    "code": 204,
    "message": "OK"
}
```
## Si el producto no existe 
```json
{
    "code": 400,
    "message": "PRODUCT_NOT_FOUND",
    "errors": []
}
```

## Si  el producto esta en uso no se puede eliminar
```json
{
    "code": 400,
    "message": "PRODUCT_USED",
    "errors": []
}
```
## Estructura de la Respuesta

| Propiedad | Tipo    | Descripción
| ---       | ---     | ---
| code      | integer | Código HTTP
| message   | string  | Descripción del código HTTP
